import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { WeatherService } from '../../services/weather.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {
  weatherSearch: FormGroup;
  weatherData: any;
  weatherGeoData: any;
  weatherDescription: any;
  weatherGeoDescription: any;
  lat: number;
  lon: number;
  currentDate = new Date();
  dateTime: string;

  constructor(private formBuilder: FormBuilder, private weatherService: WeatherService) { }

  ngOnInit(): void {
    this.weatherSearch = this.formBuilder.group({
      location: ['']
    });
    this.getLocation();

    this.dateTime = this.currentDate.getDate() + '/'
                + (this.currentDate.getMonth() + 1)  + '/'
                + this.currentDate.getFullYear() + ' @'
                + this.currentDate.getHours() + ':'
                + this.currentDate.getMinutes() + ':'
                + this.currentDate.getSeconds();
  }

  // tslint:disable-next-line:typedef
  onSubmit(value) {
    this.weatherService
      .getWeather(value.location)
      .subscribe(data => {
        this.weatherData = data;
        this.weatherDescription = this.weatherData?.list[0].weather[0].main;
        console.log(this.weatherData);
      }, (error) => {
        console.log(error);
      });
  }

  // tslint:disable-next-line:typedef
  getLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.watchPosition(data => {
        this.lat = data.coords.latitude;
        this.lon = data.coords.longitude;

        this.weatherService.getWeatherByCords(this.lat, this.lon)
          // tslint:disable-next-line:no-shadowed-variable
          .subscribe(data => {
            this.weatherGeoData = data;
            this.weatherGeoDescription = this.weatherGeoData?.weather[0].main;
            console.log(this.weatherGeoDescription);
          });
      });
    }
  }
}
