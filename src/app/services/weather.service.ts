import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  apiKey = '434ec2d523a29dc7f174a616452628d5';
  apiUrl = 'http://api.openweathermap.org/data/2.5';

  constructor(private http: HttpClient) {
  }

  getWeather(loc: string): Observable<any> {
    return this.http.get(
      `${this.apiUrl}/find?q=${loc}&units=metric&APPID=${this.apiKey}`
    ).pipe(
      catchError(this.handleError)
      );
  }

  // tslint:disable-next-line:typedef
  getWeatherByCords(lat, lon) {
    const params = new HttpParams()
      .set('lat', lat)
      .set('lon', lon)
      .set('units', 'metric')
      .set('appid', this.apiKey);

    return this.http.get(this.apiUrl + '/weather', { params });
  }

  // tslint:disable-next-line:typedef
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {

      // The backend returned an unsuccessful response code.
      console.error(
        `Backend returned code ${error.status}`
        );
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Could not get the data from the server.');
  }
}

